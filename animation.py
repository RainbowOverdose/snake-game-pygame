import pygame, sys
from pygame.locals import *

pygame.init()

FPS = 30 # frames per second setting
fpsClock = pygame.time.Clock()

colorPrincipal = pygame.Color(32,178,170)
BLACK = (10, 10, 10)
WHITE = (235, 235, 235)

# set up the window
DISPLAYSURF = pygame.display.set_mode((600, 400), 0, 32)
pygame.display.set_caption('Animation')


catImg = pygame.image.load('cat.png')
catx = 10
caty = 10
direction = 'right'

fontObj = pygame.font.Font('DKBlueSheep.otf', 24)
txtSurface = fontObj.render('Hello brave new world!', True, colorPrincipal)
txtRectSurface = txtSurface.get_rect()
txtRectSurface.center = (300, 200)

pygame.mixer.music.load('buddy.mp3')
pygame.mixer.music.set_volume(0.15)
pygame.mixer.music.play(-1, 15)

while True: # the main game loop
    DISPLAYSURF.fill(WHITE)
    DISPLAYSURF.blit(txtSurface, txtRectSurface)
    if direction == 'right':
        catx += 3
        if catx >= 545:
            direction = 'down'
    elif direction == 'down':
        caty += 3
        if caty >= 345:
            direction = 'left'
    elif direction == 'left':
        catx -= 3
        if catx <= 10:
            direction = 'up'
    elif direction == 'up':
        caty -= 3
        if caty <= 10:
            direction = 'right'

    DISPLAYSURF.blit(catImg, (catx, caty))

    for event in pygame.event.get():
        if event.type == QUIT:
            pygame.quit()
            sys.exit()
    pygame.display.update()
    fpsClock.tick(FPS)
